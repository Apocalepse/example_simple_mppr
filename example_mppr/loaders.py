# -*- coding: utf-8 -*-
import datetime
from time import mktime

import feedparser
from simple_mppr.manager import Manager

from . import settings
from . import models


class _SyncFromRSS:
    """Реализует логику загрузки Posts, Tags и Users из rss фидов
    Потому, что 90% логики для фидов идентичны, да же названия полей на источнике

    """
    _post2model = {
        'title': 'title',
        'source_id': 'id',
        'description': 'summary',
        'pubDate': 'published_parsed',
    }

    @property
    def post2model(self):
        return self._post2model

    @property
    def feed_url(self):
        raise NotImplementedError

    def sync(self):
        db_manager = Manager(settings.SQLITE_DB_PATH)

        feed = feedparser.parse(self.feed_url)
        feed_entries = feed.entries

        for feed_entry in feed_entries:
            user_id = db_manager.upsert(models.Users(), {'name': feed_entry['author']})

            # условие для сопоставления постов
            post_cond = {'source_id': feed_entry[self.post2model['source_id']]}

            # смаппим поля таблицы с полями на источнике
            post_data = {
                field_name: feed_entry[self.post2model.get(field_name)] for field_name in models.Posts.fields
                if self.post2model.get(field_name)
            }

            post_data['pubDate'] = self._dt_from_struct_time(post_data['pubDate'])
            post_data['creator'] = user_id

            # условие сопоставления нужно так же заинсертить если что
            post_data.update(post_cond)

            post_id = db_manager.upsert(models.Posts(), post_cond, post_data)

            # запишем теги в базу и свяжем теги с постом
            for tag in feed_entry['tags']:
                tag_id = db_manager.upsert(models.Tags(), {'title': tag['term']})
                db_manager.upsert(models.PostTag(), {'post_id': post_id, 'tag_id': tag_id})

    @staticmethod
    def _dt_from_struct_time(struct_time):
        """Конвертирует struct_time в datetime

        Args:
            struct_time (time.time_struct): объект struct_time

        Returns:
            (datetime.datetime): дата и время

        """
        return datetime.datetime.fromtimestamp(mktime(struct_time))


class Habr(_SyncFromRSS):
    feed_url = 'https://habr.com/rss/hubs/all/'


class Reddit(_SyncFromRSS):
    feed_url = 'https://www.reddit.com/r/news/.rss'

    @property
    def post2model(self):
        self._post2model['pubDate'] = 'updated_parsed'

        return self._post2model

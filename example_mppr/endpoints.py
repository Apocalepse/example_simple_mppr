# -*- coding: utf-8 -*-
"""
В модуле находятся "конечные точки", методы которые в итоге нужно вызвать для получения результата
"""
import os

import sqlite3

from . import settings
from . import loaders
from . import utils


def create_db_tables():
    """Создает таблицы для проекта из фикстур

    """
    with open(os.path.join(settings.BASE_DIR, 'example_mppr/fixtures.sql')) as fixtures_file:
        logger = utils.setup_logger()

        fixtures = fixtures_file.read()
        con = sqlite3.connect(settings.SQLITE_DB_PATH)

        for i, statement in enumerate(fixtures.split(';')):
            human_i = i+1

            if not statement:
                continue

            try:
                con.execute(statement)
                logger.info("{} statement executed from fixtures".format(human_i))
            except Exception as exc:
                logger.error("Error on statement {}: {}".format(human_i, exc))

                # я подумал, что можно не и не рейзить, остальное запишем
                # raise

            con.commit()


def sync_habr():
    loaders.Habr().sync()


def sync_reddit():
    loaders.Reddit().sync()

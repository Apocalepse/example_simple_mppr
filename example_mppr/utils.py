# -*- coding: utf-8 -*-
import logging


def setup_logger():
    formatter = logging.Formatter('%(asctime)s %(levelname)s: [%(name)s] %(message)s')

    root_logger = logging.getLogger('Log')
    root_logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)

    return root_logger

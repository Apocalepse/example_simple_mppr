# -*- coding: utf-8 -*-
from simple_mppr.models import Model


class _BlogModel(Model):
    """Класс определяет общее поведение для моделей проекта

    """

    # по умолчанию названия таблиц будут аналогичны именам классов
    @property
    def table_name(self):
        return self.__class__.__name__

    # в моем приложении колонка с первичным ключем у всех называется `id`
    primary_key = 'id'


class Users(_BlogModel):
    """Пользователи, разместившие посты

    """
    fields = [
        'name',
    ]


class Tags(_BlogModel):
    """Теги

    """
    fields = [
        'title',
    ]


class PostTag(_BlogModel):
    """M2M связь постов с тегами

    """
    fields = [
        'post_id',
        'tag_id',
    ]


class Posts(_BlogModel):
    """Посты из различных источников

    """
    fields = [
        'source_id',
        'title',
        'description',
        'pubDate',
        'creator',
    ]

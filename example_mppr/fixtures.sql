CREATE TABLE IF NOT EXISTS `Posts` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    `source_id` varchar(255) NOT NULL,
    `title` varchar(255) NOT NULL,
    `description` varchar(255) NULL,
    `pubDate` DATETIME NOT NULL,
    `creator` INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS `Tags` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    `title` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `PostTag` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    `post_id` INTEGER NOT NULL,
    `tag_id` INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS `Users` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    `name` varchar(255) NOT NULL
);
